import numpy
import pylab as pl
import random
import time

MAX_DISTANCE = 2.0

class SimpleEnvironment(object):
    
    def __init__(self, herb):
        self.robot = herb.robot
        self.robot_name = self.robot.GetName()
        self.boundary_limits = [[-5., -5.], [5., 5.]]

        # add an obstacle
        table = self.robot.GetEnv().ReadKinBodyXMLFile('models/objects/table.kinbody.xml')
        self.robot.GetEnv().Add(table)
        self.env = self.robot.GetEnv()

        table_pose = numpy.array([[ 0, 0, -1, 1.0], 
                                  [-1, 0,  0, 0], 
                                  [ 0, 1,  0, 0], 
                                  [ 0, 0,  0, 1]])
        table.SetTransform(table_pose)

    def SetGoalParameters(self, goal_config, p = 0.2):
        self.goal_config = goal_config
        self.p = p
        
    def GenerateRandomConfiguration(self):
        rand_p = numpy.random.ranf()
        if (0 < rand_p < self.p):
            return self.goal_config
        elif (self.p < rand_p < 1):
            config = [0] * 2;
            lower_limits, upper_limits = self.boundary_limits
            
            #Generate Random Numbers
            x_pos = random.uniform(lower_limits[0],upper_limits[0])
            y_pos = random.uniform(lower_limits[1],upper_limits[1])

            init_pos = self.robot.GetTransform() #Copy current transfom to reset robot
            temp_pos = init_pos.copy() #Copy Current Transform -- NUMPY COPY
           
            temp_pos[0][3] = x_pos #Set X Position
            temp_pos[1][3] = y_pos #Set Y Position
           
            self.env = self.robot.GetEnv() #Get Current Environment

            #Check for Collision
            with self.env: #Lock Environment before changing robot pose
                self.robot.SetTransform(temp_pos)
                is_colliding =  self.env.CheckCollision(self.env.GetBodies()[0],self.env.GetBodies()[1])

            #Generate new positions until it is collision free
            while is_colliding:
                x_pos = random.uniform(lower_limits[0],upper_limits[0])
                y_pos = random.uniform(lower_limits[1],upper_limits[1])

                temp_pos[0][3] = x_pos #Set X Position
                temp_pos[1][3] = y_pos #Set Y Position

                with self.env: #Lock Environment before changing robot pose
                    self.robot.SetTransform(temp_pos)
                    is_colliding =  self.env.CheckCollision(self.env.GetBodies()[0],self.env.GetBodies()[1])

            with self.env: #Set Robot back to initial pose
                self.robot.SetTransform(init_pos)
            
            config = [x_pos,y_pos]        
            return numpy.array(config)

    def ComputeDistance(self, start_config, end_config):
        #
        # TODO: Implement a function which computes the distance between
        # two configurations
        #
        dist = ((end_config[0]-start_config[0])**2 + 
        (end_config[1]-start_config[1])**2 ) ** 0.5
        return dist
    
    def Extend(self, start_config, end_config, limit_d= True):

        lower_limits, upper_limits = self.boundary_limits

        x_start, x_end = start_config[0], end_config[0]
        y_start, y_end = start_config[1], end_config[1]

        if not(
            (lower_limits[0] <= x_start <= upper_limits[0]) and
            (lower_limits[0] <= x_end   <= upper_limits[0]) and
            (lower_limits[1] <= y_start <= upper_limits[1]) and
            (lower_limits[1] <= y_end   <= upper_limits[1])):
            return True,None #is_colliding,config

        #Interpolated X & Y vals
        if (x_end - x_start) == 0: #Numpy arange fails - Handle zero case
            x_interp = numpy.array([x_end]*100) 
        else:
            if (x_start < x_end):
                x_interp = numpy.arange(x_start,x_end,0.01)
            else:
                x_interp = numpy.arange(x_end,x_start,0.01)
                x_interp = x_interp[::-1] 
            
        #To have same length as x_interp
        if not(y_end - y_start == 0):#Numpy arange fails - Handle zero case
            if (y_start < y_end):
                y_step_size = abs(y_end-y_start)/float(len(x_interp))
                y_interp    = numpy.arange(y_start,y_end,y_step_size)
            else:
                y_step_size = abs(y_end-y_start)/float(len(x_interp))
                y_interp    = numpy.arange(y_end,y_start,y_step_size)
                y_interp    = y_interp[::-1]

        else:
            y_interp = numpy.array([y_end]*len(x_interp))

        #Collision Check
        self.env = self.robot.GetEnv() #Get Current Environment
        init_pos = self.robot.GetTransform()
        
        temp_pos = init_pos.copy() #Copy Current Transform

        for i in range(len(x_interp)): #Iterate through all the points
                    
            temp_pos[0][3] = x_interp[i] #Set X Position
            temp_pos[1][3] = y_interp[i] #Set Y Position
            
            #Check for Collision
            with self.env: #Lock Environment before changing robot pose
                self.robot.SetTransform(temp_pos)
                is_colliding =  self.env.CheckCollision(self.env.GetBodies()[0],self.env.GetBodies()[1])
            if is_colliding:
                #TODO : DEBUG GOAL TO NEAREST
                # return is_colliding,numpy.array([x_interp[i-1],y_interp[i-1]]) #Return Previous point
                return True,None
            if limit_d == True:
                curr_distance = self.ComputeDistance(start_config,numpy.array([x_interp[i],y_interp[i]]))
                if (curr_distance >= MAX_DISTANCE):
                    return False,numpy.array([x_interp[i-1],y_interp[i-1]])
           
        return False,end_config

    def ShortenPath(self, path, timeout=5.0):
        
    #     #PSEUDOCODE: 
    #     # while(some threshold):
    #     #     Select two nodes from path at random
    #     #     connect two nodes with a straight line
    #     #     if collision:
    #     #         continue
    #     #     else 
    #     #         replace path between two nodes by a straight line
        to_exit  = time.time() + timeout
        while (time.time() < to_exit):
            #Choose two random vertices 
            vertex_1,vertex_2 = sorted(random.sample(range(len(path)),2))

            #If consecutive indices skip
            if vertex_1+1 == vertex_2:
                continue
            
            #Check for collision
            is_colliding = self.Extend(path[vertex_1],path[vertex_2],limit_d = False)[0]
            
            #If no collision remove vertices inbetween two points
            if is_colliding == False:
                for i in range(vertex_1+1,vertex_2):
                    path[i] = None
                path = [x for x in path if x != None]
        return path

    def InitializePlot(self, goal_config):
        self.fig = pl.figure()
        lower_limits, upper_limits = self.boundary_limits
        pl.xlim([lower_limits[0], upper_limits[0]])
        pl.ylim([lower_limits[1], upper_limits[1]])
        pl.plot(goal_config[0], goal_config[1], 'gx')

        # Show all obstacles in environment
        for b in self.robot.GetEnv().GetBodies():
            if b.GetName() == self.robot.GetName():
                continue
            bb = b.ComputeAABB()
            pl.plot([bb.pos()[0] - bb.extents()[0],
                     bb.pos()[0] + bb.extents()[0],
                     bb.pos()[0] + bb.extents()[0],
                     bb.pos()[0] - bb.extents()[0],
                     bb.pos()[0] - bb.extents()[0]],
                    [bb.pos()[1] - bb.extents()[1],
                     bb.pos()[1] - bb.extents()[1],
                     bb.pos()[1] + bb.extents()[1],
                     bb.pos()[1] + bb.extents()[1],
                     bb.pos()[1] - bb.extents()[1]], 'r')
        pl.ion()
        pl.show()
        
    def PlotEdge(self, sconfig, econfig):
        pl.plot([sconfig[0], econfig[0]],
                [sconfig[1], econfig[1]],
                'k.-', linewidth=2.5)
        pl.draw()

