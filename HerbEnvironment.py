import numpy
from openravepy import databases,IkParameterization
import random
import time

#Change this to limit each edge
MAX_DISTANCE = 0.75

class HerbEnvironment(object):
    
    def __init__(self, herb):
        self.robot = herb.robot
        self.robot_name = self.robot.GetName()

        # add a table and move the robot into place
        table = self.robot.GetEnv().ReadKinBodyXMLFile('models/objects/table.kinbody.xml')
        self.robot.GetEnv().Add(table)

        table_pose = numpy.array([[ 0, 0, -1, 0.7], 
                                  [-1, 0,  0, 0], 
                                  [ 0, 1,  0, 0], 
                                  [ 0, 0,  0, 1]])
        table.SetTransform(table_pose)

        # set the camera
        camera_pose = numpy.array([[ 0.3259757 ,  0.31990565, -0.88960678,  2.84039211],
                                   [ 0.94516159, -0.0901412 ,  0.31391738, -0.87847549],
                                   [ 0.02023372, -0.9431516 , -0.33174637,  1.61502194],
                                   [ 0.        ,  0.        ,  0.        ,  1.        ]])
        self.env = self.robot.GetEnv() #Get Current Environment
        self.env.GetViewer().SetCamera(camera_pose)
        
        # goal sampling probability
        self.p = 0.0
        # Get Forward Kinematics
        self.ik = databases.inversekinematics.InverseKinematicsModel(self.robot,iktype=IkParameterization.Type.Translation3D)
        #Get Joint Weights 
        self.joint_weights = self.robot.GetActiveDOFWeights()
        self.init_pos = self.robot.GetActiveDOFValues()


    def SetGoalParameters(self, goal_config, p = 0.2):
        self.goal_config = goal_config
        self.p = p
        

    def GenerateRandomConfiguration(self):
        rand_p = numpy.random.ranf()
        if (0 < rand_p < self.p):
            return self.goal_config
        elif (self.p < rand_p < 1):
            config = [0] * len(self.robot.GetActiveDOFIndices())
            lower_limits, upper_limits = self.robot.GetActiveDOFLimits()

        #Generate Raom Numbers
        for i in range(0,len(config)):
            config[i] = random.uniform(lower_limits[i],upper_limits[i])

        #Check for Collision
        with self.env: #Lock Environment before changing robot pose
            self.robot.SetActiveDOFValues(config)
            is_colliding1 = self.env.CheckCollision(self.env.GetBodies()[0],self.env.GetBodies()[1])
            is_colliding2 = self.robot.CheckSelfCollision()
            is_colliding = is_colliding1 or is_colliding2

        #Generate new positions until it is collision free
        while is_colliding:
            for i in range(0,len(config)):
                config[i] = random.uniform(lower_limits[i],upper_limits[i])

            with self.env: #Lock Environment before changing robot pose
                self.robot.SetActiveDOFValues(config)
                is_colliding1 = self.env.CheckCollision(self.env.GetBodies()[0],self.env.GetBodies()[1])
                is_colliding2 = self.robot.CheckSelfCollision()
                is_colliding = is_colliding1 or is_colliding2
                 
        with self.env: #Set Robot back to initial pose
            self.robot.SetActiveDOFValues(self.init_pos)       
        return numpy.array(config)

    
    def ComputeDistance(self, start_config, end_config,use_norm = False):
        
        if use_norm == True:
            #Comment next to ignore weights
            start_config = start_config * self.joint_weights
            end_config   = end_config   * self.joint_weights
            return numpy.linalg.norm(start_config - end_config,1)
        else:
            with self.env:
                self.robot.SetActiveDOFValues(start_config)
            start_pos = self.ik.manip.GetTransform()[0:3,3] #Get 3D Position of End Effector
            with self.env:
                self.robot.SetActiveDOFValues(end_config)
            end_pos = self.ik.manip.GetTransform()[0:3,3] #Get 3D Position of End Effector
            with self.env:
                self.robot.SetActiveDOFValues(self.init_pos)
            return numpy.linalg.norm(start_pos - end_pos)


    def Extend(self, start_config, end_config,limit_d = True):
        
        sample = 100
        config_interp = numpy.zeros((sample,7))

        #Interpolation of X and Y values
        for i in xrange(0,len(start_config)):
            config_interp[:,i] = numpy.linspace(start_config[i], end_config[i], num=sample)

        for i in xrange(0,sample):
            temp_pos = config_interp[i][:]

            #Check for collision
            with self.env:
                self.robot.SetActiveDOFValues(temp_pos)
                is_colliding1 = self.env.CheckCollision(self.env.GetBodies()[0],self.env.GetBodies()[1])
                is_colliding2 = self.robot.CheckSelfCollision()
                is_colliding = is_colliding1 or is_colliding2

            if is_colliding:
                return True,None

            if limit_d == True:
                curr_distance = self.ComputeDistance(start_config,temp_pos)
                if (curr_distance >= MAX_DISTANCE):
                    return False,numpy.array(temp_pos)
        return False,end_config

        
    def ShortenPath(self, path, timeout=5.0):
        to_exit = time.time()+timeout
        while (time.time() < to_exit):
            vertex_1, vertex_2 = sorted(random.sample(range(len(path)),2))
            if vertex_1+1 == vertex_2:
                continue
            is_colliding = self.Extend(path[vertex_1],path[vertex_2],limit_d = False)[0]

            if is_colliding == False:
                for i in range(vertex_1+1,vertex_2):
                    path[i] = None
            path = [x for x in path if x != None]
        return path
