import numpy, operator
from RRTPlanner import RRTTree

class RRTConnectPlanner(object):

    def __init__(self, planning_env, visualize):
        self.planning_env = planning_env
        self.visualize = visualize
        
        #Intialize plot only for pr2
        if (self.planning_env.robot_name == "pr2"):
            self.to_plot = True
        else:
            self.to_plot = False
          

    def Plan(self, start_config, goal_config, epsilon = 0.001):
        
        ftree = RRTTree(self.planning_env, start_config)
        rtree = RRTTree(self.planning_env, goal_config)
        plan = []

        if self.to_plot:
            if self.visualize and hasattr(self.planning_env, 'InitializePlot'):
                self.planning_env.InitializePlot(goal_config)
        
        #Set Goal Parameters with P = 0 (Default P=0.2)
        self.planning_env.SetGoalParameters(goal_config,0)

        #Initialize nearest_start with start_config
        nearest_start = start_config.copy()

        #Initialize nearest_goal with goal_config
        nearest_goal = goal_config.copy()

        #Initialize for while loop 1st Iteration
        extend_final = start_config.copy()
        extend_goal = goal_config.copy()

        #While vertices connect with collision
        while not (extend_final==extend_goal).all(): #till ends of both trees do not meet without collision
        #TODO: EXPAND NODES INDIVIDUALLY
            p_extend_final = extend_final.copy()
            p_extend_goal  = extend_goal.copy()
            #START TREE
            #Get Next Vertex  to expand to for Start_Tree
            target_start = self.planning_env.GenerateRandomConfiguration()
            target_goal = target_start
            #Find the visited vertex closest to the new target for Start_Tree
            nearestID_start,nearest_start = ftree.GetNearestVertex(target_start)
            #Get the point closest to the start_target without collision
            extend_start = self.planning_env.Extend(nearest_start,target_start)[1]
            #If there is a feasible path
            if extend_start is not None:
                #Add vertex to start tree and get its ID
                newID_start = ftree.AddVertex(extend_start)
                ftree.AddEdge(nearestID_start,newID_start) #Join two vertices
                if self.to_plot:
                    self.planning_env.PlotEdge(ftree.vertices[nearestID_start],ftree.vertices[newID_start]) 
            else:
                #Skip this vertex if not feasible path is found
                continue

            #GOAL TREE
            #Get Next Vertex  to expand to for Goal_Tree
            #target_goal = self.planning_env.GenerateRandomConfiguration()
            #Find the visited vertex closest to the new target for Goal_Tree
            nearestID_goal,nearest_goal = rtree.GetNearestVertex(target_goal)
            #Get the point closest to the goal_target without collision
            extend_goal = self.planning_env.Extend(nearest_goal,target_goal)[1]
             #If there is a feasible path
            if extend_goal is not None:
                #Add vertex to goal tree and get its ID
                newID_goal = rtree.AddVertex(extend_goal)
                rtree.AddEdge(nearestID_goal,newID_goal) #Join two vertices
                if self.to_plot:
                    self.planning_env.PlotEdge(rtree.vertices[nearestID_goal],rtree.vertices[newID_goal]) 
            else:
                extend_goal = p_extend_goal.copy()
                continue

            
            extend_final = self.planning_env.Extend(extend_start,extend_goal)[1]
            if extend_final is not None:
                pass
            else:
                extend_final = p_extend_final.copy()


        #BACKTRACKING
        # tree.edges structure --> { Vertex ID : Parent/Start Vertex ID}
        #Get the ID for the goal node
        prev_ID_start = ftree.edges.keys()[-1]
        prev_ID_goal = rtree.edges.keys()[-1]
        #Get Vertex at the ID
        curr_vertex_start = ftree.vertices[prev_ID_start]
        curr_vertex_goal = rtree.vertices[prev_ID_goal]
        #Append Vertex to the plan
        plan.append(ftree.vertices[prev_ID_start])
        
        if self.to_plot:
            if self.visualize and hasattr(self.planning_env, 'InitializePlot'):
                self.planning_env.InitializePlot(goal_config)

        while not (curr_vertex_start == start_config).all():
            next_ID_start     = ftree.edges[prev_ID_start] #Get Next ID
            curr_vertex_start = ftree.vertices[next_ID_start]  #Get Vertex at ID
            if self.to_plot:
                self.planning_env.PlotEdge(ftree.vertices[prev_ID_start],curr_vertex_start)  
            plan.append(curr_vertex_start)
            prev_ID_start     = next_ID_start

        plan = plan[::-1]
        if self.to_plot:
            self.planning_env.PlotEdge(ftree.vertices[ftree.edges.keys()[-1]],rtree.vertices[prev_ID_goal])
        plan.append(rtree.vertices[prev_ID_goal])
        
        while not (curr_vertex_goal == goal_config).all():
            next_ID_goal = rtree.edges[prev_ID_goal] #Get Next ID
            curr_vertex_goal = rtree.vertices[next_ID_goal]  #Get Vertex at ID
            if self.to_plot:
                self.planning_env.PlotEdge(rtree.vertices[prev_ID_goal],curr_vertex_goal)  
            plan.append(curr_vertex_goal)
            prev_ID_goal = next_ID_goal

        return plan
