#!/usr/bin/env python

import argparse, numpy, openravepy, time

from HerbRobot import HerbRobot
from SimpleRobot import SimpleRobot
from HerbEnvironment import HerbEnvironment
from SimpleEnvironment import SimpleEnvironment
from RRTPlanner import RRTPlanner
from RRTConnectPlanner import RRTConnectPlanner

import IPython
import time

def main(robot, planning_env, planner,visualize):

    raw_input('Press any key to begin planning')
    print "Starting to Plan"
    start_config = numpy.array(robot.GetCurrentConfiguration())
    if robot.name == 'herb':
        goal_config = numpy.array([ 3.68, -1.90,  0.00,  2.20,  0.00,  0.00,  0.00 ])
    else:
        goal_config = numpy.array([2.0, 0.0])

    print "Planning..."
    start_time = time.time()
    plan = planner.Plan(start_config, goal_config)
    time_elapsed = time.time() - start_time

    print "Number of Vertices" , len(plan)
    print "Plan :" ,plan
    print "Distance Travelled", computeDistanceTravelled(planning_env,plan)
    print "Time Taken: ", time_elapsed
   
    #Uncomment next line To Move without path shortening
    # traj1 = robot.ConvertPlanToTrajectory(plan)
    # robot.ExecuteTrajectory(traj1)

    print "Shortening Path... Timeout 5 seconds"    
    plan_short = planning_env.ShortenPath(plan)
    print "Short Plan Length" , len(plan_short)
    print "Short Plan :" ,plan_short
    print "Distance Travelled", computeDistanceTravelled(planning_env,plan_short)

    #TO Plot shortened Path
    if visualize and hasattr(planning_env, 'InitializePlot'):
        planning_env.InitializePlot(goal_config)

        for i in range(len(plan_short)-1):
            planning_env.PlotEdge(plan_short[i],plan_short[i+1]) 

    #uncomment next line To Move with path shortening 
    # traj2 = robot.ConvertPlanToTrajectory(plan_short)
    
    # robot.ExecuteTrajectory(traj2)

def computeDistanceTravelled(planning_env,plan):
    distance = 0
    for i in range(len(plan)-1):
        distance += planning_env.ComputeDistance(plan[i],plan[i+1])
    return distance


if __name__ == "__main__":
    
    parser = argparse.ArgumentParser(description='script for testing planners')
    
    parser.add_argument('-r', '--robot', type=str, default='simple',
                        help='The robot to load (herb or simple)')
    parser.add_argument('-p', '--planner', type=str, default='rrt',
                        help='The planner to run (rrt or rrtconnect)')
    parser.add_argument('-m', '--manip', type=str, default='right',
                        help='The manipulator to plan with (right or left) - only applicable if robot is of type herb')
    parser.add_argument('-v', '--visualize', action='store_true',
                        help='Enable visualization of tree growth (only applicable for simple robot)')
    parser.add_argument('-d', '--debug', action='store_true',
                        help='Enable debug logging')
    

    args = parser.parse_args()
    
    openravepy.RaveInitialize(True, level=openravepy.DebugLevel.Info)
    openravepy.misc.InitOpenRAVELogging()

    if args.debug:
        openravepy.RaveSetDebugLevel(openravepy.DebugLevel.Debug)

    env = openravepy.Environment()
    env.SetViewer('qtcoin')
    env.GetViewer().SetName('Homework 2 Viewer')

    # First setup the environment and the robot
    visualize = args.visualize
    if args.robot == 'herb':
        robot = HerbRobot(env, args.manip)
        planning_env = HerbEnvironment(robot)
        visualize = False
    elif args.robot == 'simple':
        robot = SimpleRobot(env)
        planning_env = SimpleEnvironment(robot)

    else:
        print 'Unknown robot option: %s' % args.robot
        exit(0)

    # Next setup the planner
    if args.planner == 'rrt':
        planner = RRTPlanner(planning_env, visualize=visualize)
    elif args.planner == 'rrtconnect':
        planner = RRTConnectPlanner(planning_env, visualize=visualize)
    else:
        print 'Unknown planner option: %s' % args.planner
        exit(0)
    
    main(robot, planning_env, planner,visualize)
    IPython.embed()

        
    
